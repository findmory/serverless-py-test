## Steps for setting up Serverless with python

create the basics

```
serverless create
    --template aws-python3
    --name catfacts
```

deploy it:

```
serverless deploy --stage dev
```

Edit the .yaml

```
provider:
  name: aws
  runtime: python3.6
  profile: serverless #this is your profile name in ~/.aws/credentials

functions:
  show:
    # the "show" part of show.handler is the file,
    # and "handler" indicates the function
    handler: show.handler
    events:
      - http:
          # instead of / you can define any HTTP path you like
          # since we just have one endpoint I used /
          path: /
          method: get
```

### install stuff to test local

```
npm init
npm install serverless-offline-python serverless-python-requirements --save-dev
```

edit serverless.yml

```
plugins:
  - serverless-offline-python
```

run it

```
serverless offline start
```

## to include packages

```
npm install --save serverless-python-requirements
```

create a `requirements.txt` file and then add this plugin to the .yml

```
plugins:
  - serverless-python-requirements
```
